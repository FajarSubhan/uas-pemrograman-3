// import bumbu-bumbu yang diperlukan
import java.sql.*;
import java.util.Scanner;

public class Mhs
{

    static Connection conn;
    static PreparedStatement prepared;
    static Statement stmt;
    static ResultSet rs;

    public static void main(String[] args)
    {

        try
        {
            Class.forName("org.sqlite.JDBC");
            Scanner scn  = new Scanner(System.in);

            conn = DriverManager.getConnection("jdbc:sqlite:fajarsubhandb");

            // buat object statement
            stmt = conn.createStatement();

            System.out.println("- Ubah Data Nilai Mahasiswa -");

            System.out.print("Input NPM    : ");
            // untuk naro nilai nim nye
            Integer npm = scn.nextInt();

            // query sql
            String sql = "SELECT * FROM mahasiswa a WHERE a.NPM = " + npm;

            rs = stmt.executeQuery(sql);

            System.out.println("- Hasil pencarian data -");

            while(rs.next())
            {
                System.out.println("Nama            : " + rs.getString("Nama"));
                System.out.println("kelas           : " + rs.getString("Kelas"));
                System.out.println("Nilai Teori     : " + rs.getString("teori"));
                System.out.println("Nilai Praktik   : " + rs.getString("praktikum"));
            }

            System.out.println("================================================================");

            // Bagian update data berdasarkan NIM,karena field NIM sifatnya unique
            System.out.println("-Input Perubahan data-");

            System.out.printf("%s","Input Nama        : ");
            String nama     = scn.next();

            System.out.print("Input Kelas       : ");
            String kelas    = scn.next();

            System.out.print("Nilai Teori       : ");
            Integer NTeori  = scn.nextInt();

            System.out.print("Nilai Praktik     : ");
            Integer NPraktik= scn.nextInt();

            // Proses update data mahasiswa
            String UPDATE = "UPDATE mahasiswa " +
                    "SET " +
                    "Nama           = ?,"  +
                    "Kelas          = ?," +
                    "teori          = ?," +
                    "praktikum      = ? " +
                    " WHERE NPM = ? " ;


            PreparedStatement x = conn.prepareStatement(UPDATE);

            x.setString(1,nama);
            x.setString(2,kelas);
            x.setInt(3,NTeori);
            x.setInt(4,NPraktik);
            x.setInt(5,npm);
            x.executeUpdate();

            System.out.println("==================================================================");
            System.out.println("                          DATA NILAI MAHASISWA");
            System.out.println("==================================================================");

            System.out.print("NPM       Nama  Mahasiswa       Kelas   Teori   Praktirkum   Nilai Akhir   Keterangan");

            // query sql
            String sql2 = "SELECT * FROM mahasiswa WHERE NPM = " + npm;

            rs = stmt.executeQuery(sql2);

            System.out.println();

            while(rs.next())
            {
                Float nilaiakhir = ((rs.getFloat(4) * 30) / 100) + ((rs.getFloat(5) * 70) / 100);
                String desc;
                if(nilaiakhir >= 70)
                {
                    desc = "LULUS";
                }
                else
                {
                    desc = "TIDAK LULUS";
                }

                System.out.println(
                        rs.getString(1) + "\t\t\t"  +
                        rs.getString(2) + "\t\t\t\t" +
                        rs.getString(3) + "\t\t " +
                        rs.getString(4) + "\t\t\t" +
                        rs.getString(5) + "\t\t\t" +
                        nilaiakhir + "\t\t" +
                        desc
                        );
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
